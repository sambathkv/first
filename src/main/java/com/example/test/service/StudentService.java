package com.example.test.service;

import com.example.test.model.Student;
import com.example.test.repository.StudentRepository;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentService {
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})


    @Autowired
    private StudentRepository studentRepository;

    public List<Student> getAll() {
        List<Student> studentlist = new ArrayList<>();
        studentRepository.findAll().forEach(studentlist::add);
        return studentlist;
    }

    public String deleteStudent(int id) {

        Student student = studentRepository.getOne(id);
        studentRepository.delete(student);
        return "deleted";

    }

    public Student createNew(Student student) {
        return studentRepository.save(student);

    }

    public Student updateStudent(int id, Student student) {
        Student stud = studentRepository.getOne(id);
        stud.setFname(student.getFname());
        stud.setLname(student.getLname());
        stud.setAge(student.getAge());
        return studentRepository.save(stud);
    }

    public List<Student> getByKey(String key) {
        List<Student> list = new ArrayList<>();
        studentRepository.findByKey(key).forEach(list::add);
        return list;
    }

    public Student getOneStudent(int id) {
        return studentRepository.getOne(id);
    }

    public List<Object[]> getFullName() {
        return studentRepository.getFullName();
    }


}
