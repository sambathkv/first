package com.example.test.repository;

import com.example.test.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface StudentRepository extends JpaRepository<Student, Integer> {

//@Query(value="select * from student where fname like %?1 or lname like %?1",nativeQuery = true)
    //   List<Student> findByKey(String key);

    @Query(value = "select * from student where fname like %:key% or lname like %:key%", nativeQuery = true)
    List<Student> findByKey(@Param("key") String keyword);

    @Query(value = "select fname,lname from student", nativeQuery = true)
    List<Object[]> getFullName();

    @Query(value = "select count(*) from student", nativeQuery = true)
    int countStudents();

}
