package com.example.test.controller;

import com.example.test.model.Student;
import com.example.test.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @RequestMapping("/list")
    public List<Student> getAllStudents() {
        return studentService.getAll();

    }

    @RequestMapping("/delete/{id}")
    public String deleteStudent(@PathVariable("id") int id) {
        return studentService.deleteStudent(id);
    }

    @RequestMapping("/create")
    public Student createStudent(@Validated @RequestBody Student student) {
        return studentService.createNew(student);

    }

    @RequestMapping("/update/{id}")
    public Student updateStudent(@PathVariable("id") int id, @RequestBody Student student) {
        return studentService.updateStudent(id, student);
    }

    @RequestMapping("/search")
    public List<Student> searchStudent(@RequestParam("keyword") String key) {
        return studentService.getByKey(key);
    }


    @RequestMapping("/list/{id}")
    public Student getDetail(@PathVariable("id") int id) {
        return studentService.getOneStudent(id);
    }

    @RequestMapping("/fullname")
    public List<Object[]> getFullname() {
        return studentService.getFullName();
    }

}
