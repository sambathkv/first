package com.example.test.kafka.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
@EnableScheduling
public class Producer {
    @Autowired
    private KafkaTemplate<String,Integer> kafkaTemplate;


    @Scheduled(fixedRate = 10000)
    public void post() {
        Random rand = new Random();
        Integer num = rand.nextInt();
        kafkaTemplate.send("RandomNumber", num);
    }
}
