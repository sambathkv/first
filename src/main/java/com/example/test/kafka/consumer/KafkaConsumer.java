package com.example.test.kafka.consumer;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {
    @KafkaListener(topics = "RandomNumber", groupId = "group_id")
    public void consume(Integer number) {
        System.out.println("Consumed number : " + number);
    }
}
